<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
function numberToString($a) {
    $number = ""; $first =array ("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten");
        $second = array ("eleven", "twelve", "thirteen", "fourteen", "fifteen","sixteen", "seventeen", "eighteen", "nineteen");
        $third = array ("twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety");
    $fourth = array ("hundred", "thousand", "million");
    function main($a,$first, $second, $third, $fourth) {
        if ($a >= 0 && $a < 100) {
            return zeroToHundred($a,$first, $second, $third, $fourth);
        }
        if ($a >= 100 && $a < 999) {
            return hundredToThousand($a,$first, $second, $third, $fourth);
        }
        if ($a >= 1000 && $a < 1000000) {
            return thousandToMillion($a,$first, $second, $third, $fourth);
        }
        if ($a >= 1000000 && $a < 1000000000) {
            return MillionToMiliard($a,$first, $second, $third, $fourth);
        }
        if ($a >= 1000000000) {
            return Billion($a,$first, $second, $third, $fourth);
        }
    }

    function zeroToHundred($a,$first, $second, $third, $fourth) {
        if ($a >= 0 && $a <= 10) {
            return $first[$a];
        } else if ($a > 10 && $a < 20) {
            return $second[$a - 11];
        } else if ($a >= 20 && $a < 100) {
            $b = intval($a / 10);

            $c = $a - $b * 10;

            if ($c == 0) {
                return $third[$b - 2];
            } else{
                return $third[$b - 2]. " " . $first[$c];
            }

        }
    }

    function hundredToThousand($a,$first, $second, $third, $fourth) {
        $b = intval($a / 100);
        $a = $a - $b * 100;
        if ($a == 0) {
            return $first[$b] . " hundred ";
        } else
            return $first[$b] . " hundred " . main($a,$first, $second, $third, $fourth);
    }

    function thousandToMillion($a,$first, $second, $third, $fourth) {
        $b = intval($a / 1000);
        $a = $a - $b * 1000;
        if ($a == 0) {
            return main($b,$first, $second, $third, $fourth) . " thousand ";
        } else
            return main($b,$first, $second, $third, $fourth) . " thousand " . main($a,$first, $second, $third, $fourth);
    }

    function MillionToMiliard($a,$first, $second, $third, $fourth) {
        $b = intval($a / 1000000);
        $a = $a - $b * 1000000;
        if ($a == 0) {
            return main($b,$first, $second, $third, $fourth) . " million ";
        } else
            return main($b,$first, $second, $third, $fourth) . " million " . main($a,$first, $second, $third, $fourth);
    }

    function Billion($a,$first, $second, $third, $fourth) {
        $b = intval($a / 1000000000);
        $a = $a - $b * 1000000000;
        if ($a == 0) {
            return main($b,$first, $second, $third, $fourth) . " billion ";
        } else
            return main($b,$first, $second, $third, $fourth) . " billion " . main($a,$first, $second, $third, $fourth);
    }
    $number = main($a,$first, $second, $third, $fourth);
    return $number;
}

echo '11298 to string is : '.numberToString(11298);

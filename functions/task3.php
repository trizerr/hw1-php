<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);
function stringToNumber($text) {
    $number = 0;
    $first = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"];
    $second = ["eleven", "twelve", "thirteen", "fourteen", "fifteen", "seventeen", "eighteen", "nineteen"];
    $third = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];
    $fourth = ["hundred", "thousand", "million"]; $temp = 0;
    $words = explode(' ', $text);
    $wordsReverse = array_reverse($words);

    function main($wordsReverse,$words,$first,$second,$third,$fourth,$number) {
        $hundred = [];
        for ($i = 0; $i < count($wordsReverse); $i++) {
            for ($j = 0; $j < 3; $j++) {
                if ($wordsReverse[$i] == $fourth[$j]) {
                    if ($wordsReverse[$i] == "hundred") {
                        for ($t = 0; $t < count($wordsReverse) - $i - 1; $t++) {
                            $hundred[$t] = $wordsReverse[$t + $i + 1];
                        }
                        $temp = Tohundred($hundred,$first,$second,$third);
                        array_splice($wordsReverse,2,2);
                        $number += $temp + Tohundred($wordsReverse,$first,$second,$third);

                    }
                }
            }
        }
        return $number;
    }

    function Tohundred($wordsReverse,$first,$second,$third) {
        $result = 0;
        for ($i = 0; $i < count($wordsReverse); $i++) {
            for ($j = 0; $j <= 10; $j++) {
                if ($wordsReverse[$i] == $first[$j])
                    $result += $j;
            }
            for ($j = 0; $j <= 7; $j++) {
                if ($wordsReverse[$i] == $second[$j])
                    $result += ($j + 11);
            }
            for ($j = 0; $j <= 7; $j++) {
                if ($wordsReverse[$i] == $third[$j])
                    $result += (($j + 2) * 10);
            }
        }
        return $result;
    }

    return  main($wordsReverse,$words,$first,$second,$third,$fourth,$number);;
}

echo "thirty hundred two = ".stringToNumber("thirty hundred two");